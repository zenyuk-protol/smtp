###
smtp call from telnet

run telnet, then commands

OPEN 127.0.0.1 25

MAIL FROM: <bla@gmail.com>

RCPT TO: <bla@gmail.com>

DATA 
354 Enter message, ending with "." on a line by itself
Subject: test

This is a test message. //EMPTY LINE BETWEEN SUBJECT AND BODY, DOT AT THE END
.

QUIT
e.g.:

$ telnet
telnet> OPEN 127.0.0.1 25
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.
220 78bc80ae4b35 ESMTP Exim 4.92 Mon, 23 Mar 2020 04:26:23 +0000

MAIL FROM: <bla@gmail.com>
250 OK

RCPT TO: <bla@gmail.com>
250 Accepted

DATA 
354 Enter message, ending with "." on a line by itself
Subject: test

This is a test message. //EMPTY LINE BETWEEN SUBJECT AND BODY, DOT AT THE END
.
250 OK id=1jGEgi-00004R-3c

QUIT
221 78bc80ae4b35 closing connection
Connection closed by foreign host.
###

